import os
from boto3.session import Session

current_directory = os.getcwd()

S3_BUCKET = 'nexscope-safety'
S3_UPLOADPATH = 'data/2018/06/07/unstructured/processed/'
SOURCE_LOCATION = os.path.join(current_directory, r'processed/')

def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key ='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'
    region_name = 'us-east-2'
    
    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session


def get_s3_client(new=False):
    session = get_aws_session(new)

    return session.client('s3')

# get an access token, local (from) directory, and S3 (to) directory
# from the command-line

client = get_s3_client()

# enumerate local files recursively
for root, dirs, files in os.walk(SOURCE_LOCATION):

  for filename in files:

    # construct the full local path
    local_path = os.path.join(root, filename)

    # construct the full Dropbox path
    relative_path = os.path.relpath(local_path, SOURCE_LOCATION)
    s3_path = os.path.join(S3_UPLOADPATH, relative_path)

    # relative_path = os.path.relpath(os.path.join(root, filename))

    print('Searching "%s" in "%s"' % (s3_path, S3_BUCKET))
    try:
        client.head_object(Bucket=S3_BUCKET, Key=s3_path)
        print("Path found on S3! Skipping %s..."% s3_path)

        # try:
            # client.delete_object(Bucket=bucket, Key=s3_path)
        # except:
            # print "Unable to delete %s..." % s3_path
    except:
        print("Uploading %s..." % s3_path)
        client.upload_file(local_path, S3_BUCKET, s3_path)
