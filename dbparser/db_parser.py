import csv
import json
import re
import os
import errno
from boto3.session import Session

bucket = 'nexscope-safety'
path = 'data/2018/04/12/structured/processed/dailymed/deltacon/'
drugbank_dictionary = {}

def import_csv():
    #with open("/home/tester/users/abhinay/drugbank_parser/drugbank_output_abhi.csv", mode='r') as infile:
    current_directory = os.getcwd()
    file = os.path.join(current_directory, "drugbank_output_abhi.csv")
    with open(file, "r") as infile:
        reader = csv.reader(infile)
        drugbank_dictionary = dict(row[:2] for row in reader if row)
        drugbank_dictionary = dict((k, v.lower()) for k, v in drugbank_dictionary.items())
    return drugbank_dictionary

drugbank_dictionary = import_csv()

################################# S3 Download to A Folder ##############################################

def get_download_location():
    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'Dailymed_downloads/')
    if not os.path.exists(directory):
        os.mkdir(directory)
    return directory

def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session

def get_s3_client(new=False):
    session = get_aws_session(new)

    return session.client('s3')

def assert_dir_exists(path):
    """
    """
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def download_dir(client, bucket, path, destination):

    # Handle missing / at end of prefix
    if not path.endswith('/'):
        path += '/'

    paginator = client.get_paginator('list_objects_v2')
    for result in paginator.paginate(Bucket=bucket, Prefix=path):
        #print(result)
        # Download each file individually
        for key in result['Contents']:
            # Calculate relative path
            rel_path = key['Key'][len(path):]
            # Skip paths ending in /
            if not key['Key'].endswith('/'):
                local_file_path = os.path.join(destination, rel_path)
                # Make sure directories exist
                local_file_dir = os.path.dirname(local_file_path)
                assert_dir_exists(local_file_dir)
                client.download_file(bucket, key['Key'], local_file_path)


#client = get_s3_client() #Setting up the S3 connection to download directories



######################################## Processing of downloaded directory files ########################################

def json_parse_to_string(file_name):
    raw_data = json.load(open(file_name), encoding="utf8")
    raw_data_string = ', '.join("{!s}={!r}".format(k, v) for (k, v) in raw_data.items())
    clean_string = re.sub('\W+', " ", raw_data_string)
    return clean_string

def find_matching_drugbank_drugs(clean_string, drugbank_dictionary):
    
    word_list = clean_string.lower().split()
    matches_dictionary = dict((k, v) for k, v in drugbank_dictionary.items() if v in word_list)
    return matches_dictionary

def create_output_json(matching_dictionary, file_name):
    dict_keys = ['filename', 'source', 'drugnames', 'drug_ids']

    dict_values = [file_name.split('/')[-1], path.split("/")[-3], [v for k,v in matching_dictionary.items()],
                   [k for k,v in matching_dictionary.items()]]

    final_dictionary = dict(zip(dict_keys, dict_values))
    #Saving in a results folder

    current_directory = os.getcwd()
    output_folder = os.path.join(current_directory, r'db_parsed_results')
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    with open(os.path.join(output_folder, file_name.split('/')[-1]), 'w') as f:
        json.dump(final_dictionary, f)

def integration_pieces(downloaded_directory):
    # Iterate over the files in the input directory to carry out the below processes
    for file_name in os.listdir(downloaded_directory):
        #step 1 - parse the file and get clean string
        file_path = os.path.join(downloaded_directory,file_name)
        cleaned_string = json_parse_to_string(file_path)
        matching_results = find_matching_drugbank_drugs(cleaned_string, drugbank_dictionary)
        create_output_json(matching_results, file_path)

################## MAIN FUNCTION ################

def main():
    destination = get_download_location()
    client = get_s3_client()
    download_dir(client, bucket, path, destination)
    integration_pieces(destination)

main()





