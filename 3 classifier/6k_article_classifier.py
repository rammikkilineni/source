from classificationFunctions import *
import pickle
from utilities import *
import os
import shutil

if __name__ == '__main__':
    logger.info('Started Processing the classification')
    
    if check_directories():
        clf, select = pickle.load(open('NBmodelFrom6kArticles.pickle', 'rb'))

        # Read the files from local
        for file in os.listdir(DOWNLOAD_LOCATION):
            try:
                filename = os.fsdecode(file)

                with open(str(DOWNLOAD_LOCATION) + str(filename), 'r') as readfile:
                    data = readfile.read()
                
                # Logging the data
                logger.info(data)

                # Get prediction for the content
                y_predict = getPrediction(clf, select, [data])
                
                #logger.info('Prediction Result: {}'.format(str(y_predict[0])))
                logger.info('Prediction Result: {}'.format(str('[%s]' % ', '.join(map(str, y_predict)))))
                
                if y_predict[0] == 1:
                    # Copy to relevant
                    folderPath = os.path.join(RELEVANT_LOCATION, os.path.basename(filename))
                    shutil.move(str(DOWNLOAD_LOCATION) + str(filename), folderPath)
                    logger.info('Saved {} to Relevant directory'.format(folderPath))
                else:
                    # Copy to non-relevant
                    folderPath = os.path.join(NONRELEVANT_LOCATION, os.path.basename(filename))
                    shutil.move(str(DOWNLOAD_LOCATION) + str(filename), folderPath)
                    logger.info('Saved {} to Non-Relevant directory'.format(folderPath))
            except Exception as e:
                # exception happened
                logger.error('Error occured processing file: {}'.format(file))
                logger.error(e)
    else:
        logger.info('Error creating the local directories')

    logger.info('Finished procesing the classification')

    # Call the Classifier for each file
    # clf, select = pickle.load(open('NBmodelFrom6kArticles.pickle', 'rb'))
    # text_articles = pandas.read_csv('url_abstracts_final.csv')
    # y_predict = getPrediction(clf, select, text_articles['Contents'])

    # print('{} signals out of the set of {} articles'.format(sum(y_predict),len(y_predict)))
    # plotConfusionMatrix(metrics.confusion_matrix(text_articles['Relevant/Non-Relevant'],y_predict), 'confusion matrix')
    # showMostDiagnosticWords(select, clf)