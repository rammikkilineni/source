import time, logging
import shutil
import os

current_directory = os.getcwd()

DOWNLOAD_LOCATION = os.path.join(current_directory, r'processed/')
RELEVANT_LOCATION = os.path.join(current_directory, r'classified', r'relevant')
NONRELEVANT_LOCATION = os.path.join(current_directory, r'classified', r'nonrelevant')

def check_directories():
    try:          
        if not os.path.exists(DOWNLOAD_LOCATION):
            os.makedirs(DOWNLOAD_LOCATION)
        if not os.path.exists(RELEVANT_LOCATION):
            os.makedirs(RELEVANT_LOCATION)
        if not os.path.exists(NONRELEVANT_LOCATION):
            os.makedirs(NONRELEVANT_LOCATION)
        return True
    except:
        return False

def get_logger(filename):
    logging.basicConfig(filename=filename, level=logging.DEBUG,
                        format='%(asctime)s \t[%(name)-s] \t%(filename)-s \t%(lineno)-d'
                               ' \t%(levelname)-s: \t%(message)s \r',
                        datefmt='%Y-%m-%dT%T%Z')
    logger = logging.getLogger(filename)

    return logger

log_name = time.strftime("%Y-%m-%d")
logger = get_logger(log_name)