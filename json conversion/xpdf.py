import xmlformatter
import subprocess
import pdfquery
import tempfile
import zipfile
import shutil
import ntpath
import shlex
import os


libreoffice_docx_base_cmd = 'soffice --invisible --headless --infilter="writer_pdf_import" --convert-to docx:"MS Word 2007 XML"'
libreoffice_xml_base_cmd = 'soffice --invisible --headless --infilter="writer_pdf_import" --convert-to xml:"OpenDocument Text Flat XML"'


def process_pdf_to_xml(pdf_file, xmldir, processing_dir,
                       format_xml, formatter, 
                       conversion_type='libredocx'):
    pdf_file_basename = ntpath.basename(pdf_file)
    xml_file_basename = pdf_file_basename.replace('.pdf', '.xml')
    docx_file_basename = pdf_file_basename.replace('.pdf', '.docx')
    docx_file = os.path.join(processing_dir, docx_file_basename)
    xml_file = os.path.join(xmldir, xml_file_basename)

    if conversion_type == 'libredocx':
        xml_dir = os.path.join(xmldir, 'libredocx')
        if not os.path.exists(xml_dir):
            os.makedirs(xml_dir)
        xml_file = os.path.join(xml_dir, xml_file_basename)
        libreoffice_cmd = '{} {} --outdir {}'.format(
            libreoffice_docx_base_cmd,
            pdf_file,
            processing_dir)

        libreoffice_args = shlex.split(libreoffice_cmd)

        p = subprocess.Popen(libreoffice_args)
        p.communicate()

        with zipfile.ZipFile(docx_file, 'r') as indocx:
            with indocx.open('word/document.xml') as inxml:
                with open(xml_file, 'wb') as outxml:
                    shutil.copyfileobj(inxml, outxml)

        os.remove(docx_file)
    elif conversion_type == 'librexml':
        xml_dir = os.path.join(xmldir, 'librexml')
        xml_file = os.path.join(xml_dir, xml_file_basename)
        if not os.path.exists(xml_dir):
            os.makedirs(xml_dir)
        libreoffice_cmd = '{} {} --outdir {}'.format(
            libreoffice_xml_base_cmd,
            pdf_file,
            xml_dir)

        libreoffice_args = shlex.split(libreoffice_cmd)

        p = subprocess.Popen(libreoffice_args)
        p.communicate()
    elif conversion_type == 'minerxml':
        xml_dir = os.path.join(xmldir, 'minerxml')
        if not os.path.exists(xml_dir):
            os.makedirs(xml_dir)
        xml_file = os.path.join(xml_dir, xml_file_basename)
        pdf = pdfquery.PDFQuery(pdf_file)
        pdf.load()
        pdf.tree.write(xml_file, pretty_print=True, encoding="utf-8")

    if os.path.exists(xml_file):
        if format_xml:
            formatter.format_file(xml_file)
        print('Converted {} to {}'.format(pdf_file, xml_file))
        return True
    print('Conversion of {} failed'.format(pdf_file))
    return False


def pdf_to_xml(pdfdir, xmldir, processing_dir, format_xml=False):
    pdf_files = (os.path.join(prefix, file) for prefix, subdirs,
                 files in os.walk(
        pdfdir) for file in files if not
        file.startswith('.') and file.endswith('.pdf'))
    formatter = xmlformatter.Formatter()

    for pdf_file in pdf_files:
        process_pdf_to_xml(pdf_file, xmldir,
                           processing_dir, format_xml, formatter,
                           )
        process_pdf_to_xml(pdf_file, xmldir,
                           processing_dir, format_xml, formatter,
                           'librexml')
        # process_pdf_to_xml(pdf_file, xmldir,
        #                    processing_dir, format_xml, formatter,
        #                    'minerxml')


def main():
    temp_dir = tempfile.mkdtemp(dir='/tmp/processing')
    root_dir = os.path.join(os.path.expanduser('~'), 'medmeme', 'data')
    pdf_dir = os.path.join(root_dir, 'inputs', 'pdf')
    xml_dir = os.path.join(root_dir, 'inputs', 'xml')

    pdf_to_xml(pdf_dir, xml_dir, temp_dir)


if __name__ == '__main__':
    main()
