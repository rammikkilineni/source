#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 10:45:07 2018

@author: Dastgeer Shaikh
Objective - [1] Detection of source of langauage
[2] translation into final "English language"

"""

import goslate
import translate
import time
from translate import translator
from googletrans import Translator
import csv
import re



def language(srcLanguage):
    langCode = {
            'af': 'afrikaans',
            'sq': 'albanian',
            'am': 'amharic',
            'ar': 'arabic',
            'hy': 'armenian',
            'az': 'azerbaijani',
            'eu': 'basque',
            'be': 'belarusian',
            'bn': 'bengali',
            'bs': 'bosnian',
            'bg': 'bulgarian',
            'ca': 'catalan',
            'ceb': 'cebuano',
            'ny': 'chichewa',
            'zh-cn': 'chinese (simplified)',
            'zh-tw': 'chinese (traditional)',
            'co': 'corsican',
            'hr': 'croatian',
            'cs': 'czech',
            'da': 'danish',
            'nl': 'dutch',
            'en': 'english',
            'eo': 'esperanto',
            'et': 'estonian',
            'tl': 'filipino',
            'fi': 'finnish',
            'fr': 'french',
            'fy': 'frisian',
            'gl': 'galician',
            'ka': 'georgian',
            'de': 'german',
            'el': 'greek',
            'gu': 'gujarati',
            'ht': 'haitian creole',
            'ha': 'hausa',
            'haw': 'hawaiian',
            'iw': 'hebrew',
            'hi': 'hindi',
            'hmn': 'hmong',
            'hu': 'hungarian',
            'is': 'icelandic',
            'ig': 'igbo',
            'id': 'indonesian',
            'ga': 'irish',
            'it': 'italian',
            'ja': 'japanese',
            'jw': 'javanese',
            'kn': 'kannada',
            'kk': 'kazakh',
            'km': 'khmer',
            'ko': 'korean',
            'ku': 'kurdish (kurmanji)',
            'ky': 'kyrgyz',
            'lo': 'lao',
            'la': 'latin',
            'lv': 'latvian',
            'lt': 'lithuanian',
            'lb': 'luxembourgish',
            'mk': 'macedonian',
            'mg': 'malagasy',
            'ms': 'malay',
            'ml': 'malayalam',
            'mt': 'maltese',
            'mi': 'maori',
            'mr': 'marathi',
            'mn': 'mongolian',
            'my': 'myanmar (burmese)',
            'ne': 'nepali',
            'no': 'norwegian',
            'ps': 'pashto',
            'fa': 'persian',
            'pl': 'polish',
            'pt': 'portuguese',
            'pa': 'punjabi',
            'ro': 'romanian',
            'ru': 'russian',
            'sm': 'samoan',
            'gd': 'scots gaelic',
            'sr': 'serbian',
            'st': 'sesotho',
            'sn': 'shona',
            'sd': 'sindhi',
            'si': 'sinhala',
            'sk': 'slovak',
            'sl': 'slovenian',
            'so': 'somali',
            'es': 'spanish',
            'su': 'sundanese',
            'sw': 'swahili',
            'sv': 'swedish',
            'tg': 'tajik',
            'ta': 'tamil',
            'te': 'telugu',
            'th': 'thai',
            'tr': 'turkish',
            'uk': 'ukrainian',
            'ur': 'urdu',
            'uz': 'uzbek',
            'vi': 'vietnamese',
            'cy': 'welsh',
            'xh': 'xhosa',
            'yi': 'yiddish',
            'yo': 'yoruba',
            'zu': 'zulu',
            'fil': 'Filipino',
            'he': 'Hebrew'
            }
    
    return langCode[srcLanguage]




def total_word_count(data):
    wordCount = 0
    for i in range(0, len(data)):
        wordCount = wordCount + len(data[i])
            
    return wordCount



def language_translator(content_of_document):
        from prettytable import PrettyTable
        
        translator = Translator()
       
        translations = translator.translate(content_of_document, dest='en')

        translated_document = list()
        
        mytable    = PrettyTable(["Entity", "Output"])
    
        for translation in translations:
            #print(translation.origin, ' -> ', translation.text)
            trans_text    = translation.text;
            transLangName = language( DetectLang(trans_text) ).upper()
            translated_document.append(trans_text)
            
            orig_text     = translation.origin
            orig_lang     = DetectLang( orig_text )
            origLangName  = language( orig_lang ).upper()
            
           
        wc1 = total_word_count(content_of_document)
        wc2 = total_word_count(translated_document)
    
        mytable.add_row(["Origin Language", origLangName])
        mytable.add_row(["Translated Language", transLangName])
        mytable.add_row(["Word Count Original Document", wc1])
        mytable.add_row(["Word Count Translated Document", wc2])
        
        print(mytable)
    
      
        return translated_document, transLangName



        
def write_output(dirPath, FileName, transLangName, doc):    
    """
    OUTPUT /EXPORT FILE SAVED HERE - DESTINATION DOCUMENT MUST BE ENGLISH
    """
        
    outFileName = dirPath + FileName + '-' + transLangName + '.txt'
           
    with open(outFileName, "w") as output:
         writer = csv.writer(output, lineterminator='\n')
         for val in doc:
             writer.writerow([val])     
             

def DetectLang(word):
    return Translator().detect(word).lang
                
"""
************************* MAIN CODE ******************************************
"""
   
if __name__ == '__main__':
    startTime = time.time()

    dirPath      = "/home/zigbytecom/Desktop/VertivWrok/translator/"   

    """ 
    |specify input/source document|
    """

    FileName     = "02 http_www_spine_at_patienteninfo_osteoporose.txt" # German document
    #FileName     = "03 Spanish_text.txt"                       # Spanish document 
    #FileName     = "01 french_text.txt"
    #FileName     = "04 japanes_text.txt"                       # japanes document
    #FileName     = "05 Pharmacist Training Program for Immunizations.htm" # English document
    
    source_file  = dirPath + FileName;
    
    """
    READ INPUT /SOURCE FILE HERE
    """
        
      
    fh = open(source_file, 'r', errors='ignore')
    content_of_document = fh.readlines()
    fh.close()
       
    originalDocument = re.sub(r'[^\w]', ' ', str(content_of_document))
    originalLanguage = DetectLang( originalDocument )


    # check if the source is ENGLISH 
    
    if ( language(originalLanguage) != 'english' ):
        print('Source Document is NOT ENGLISH...TRANSLATE INTO ENGLISH ')
        translated_document, transLangName = language_translator(content_of_document)
        write_output(dirPath, FileName, transLangName, translated_document)
    else:
        print( "WOW - ENGLISH DOCUMENT - NOTHING NEEDS to be done " )
        
        
    endTime = time.time()
    totalTime =  (endTime - startTime )
    print('Total Execution Time (sec)        == ', round(totalTime, 4) )
        
      