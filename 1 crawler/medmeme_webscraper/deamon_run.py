import daemon
from daemon import DaemonContext

import entry

with daemon.DaemonContext():
    entry()