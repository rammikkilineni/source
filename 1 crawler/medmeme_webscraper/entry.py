from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scraper_utilities import get_logger


def main():
    logger = get_logger(__name__)
    logger.info('Crawl intiation')

    settings = get_project_settings()
    process = CrawlerProcess(settings)
    process.crawl('deltaSpider')
    process.start()

if __name__ == '__main__':
    main()