# -*- coding: utf-8 -*-
from scraper_utilities import getStartUrls
from scraper_utilities import getFilters
from scraper_utilities import getAllowedDoamins
from scraper_utilities import db_connect
from scraper_utilities import get_logger
from scraper_utilities import createfile
from scraper_utilities import uploadToS3
from scrapy.spider import CrawlSpider
from scrapy.spider import Rule
from scrapy.linkextractor import LinkExtractor
from scrapy.loader import ItemLoader
from medmeme_webscraper.items import MedmemeWebscraperItem
import socket
import datetime
import uuid
from tika import detector
import os
import psycopg2

logger = get_logger(__name__)

class DeltaspiderSpider(CrawlSpider):
    name = 'deltaSpider'
    filters = []
    
    def __init__(self):
        super(DeltaspiderSpider, self).__init__()
        self.allowed_domains = getAllowedDoamins()
        self.start_urls = getStartUrls()
        self.filters = getFilters()
        self.bucket = 'nexscope-safety'
        self.s3_root = os.path.join(
            'data',
            datetime.datetime.now().strftime('%Y/%m/%d'),
            'unstructured',
            'raw'
        )

    rules = (Rule(LinkExtractor(allow=filters), callback='parse_extract', follow=True),)

    def parse(self, response):
        domain_name = response.url.split('/')[2]
        tika_filetype = detector.from_buffer(response.body)
        filetype = tika_filetype.split('/')[1]
        try:
            conn = db_connect()
            cur = conn.cursor()
            cur.execute("select seedurl_id from seed_urls where domain_name='%s';" % domain_name)
            seedurl_id = cur.fetchall()
            for id in seedurl_id:
                cur.execute("INSERT into url_attributes(seedurl_id, url, http_status) select '%s', '%s', '%s' where not exists (select url from url_attributes where url='%s')" % (id[0], response.url, response.status, response.url))

            if 'Last-Modified' in response.headers:
                last_modified = response.headers['Last-Modified'].decode("utf-8")
                cur.execute("select last_modified from url_attributes where url = '%s';" % response.url)
                lm = cur.fetchall()
                old_last_modified = lm[0]
                if last_modified > old_last_modified:
                    cur.execute("UPDATE url_attributes set last_modified='%s' where url='%s'" % (last_modified, response.url))
            elif 'Content-Length' in response.headers:
                content_length = response.headers['Content-Length'].decode("utf-8")
                cur.execute("select content_length from url_attributes where url = '%s';" % response.url)
                cl = cur.fetchall()
                old_content_length = cl[0]
                if content_length > old_content_length:
                    cur.execute("UPDATE url_attributes set content_length='%s' where url='%s'" % (content_length, response.url))
            else:
                logger.info('No last-modified date or content-length in response header for URL' % response.url)

        except psycopg2.Error as e:
            cur.close()
            logger.info(e)
        except KeyError as e:
            logger.info(e)
            pass
        except Exception:
            pass
        finally:
            conn.commit()
            cur.close()
            conn.close()

        filename = str(uuid.uuid4())
        filex = createfile(filename, domain_name)
        filex.write(response.body)
        s3_key = os.path.join(self.s3_root, domain_name, filetype, filename)
        download_dir = os.path.join(os.path.expanduser('~'), domain_name)
        file_location = os.path.join(download_dir, filename)
        uploadToS3(file_location, self.bucket, s3_key)



        scrape_item = ItemLoader(item=MedmemeWebscraperItem(), response=response)
        scrape_item.add_value('url', response.url)
        scrape_item.add_value('project', self.settings.get('BOT_NAME'))
        scrape_item.add_value('spider', self.name)
        scrape_item.add_value('server', socket.gethostname())
        scrape_item.add_value('date', datetime.datetime.now().isoformat())
        scrape_item.add_value('schema', 'http://json-schema.org/draft-06/schema#')
        scrape_item.add_value('title', 'Website Metadata')
        scrape_item.add_value('description', 'Contains metadata of scraped URL')
        scrape_item.add_value('domain_name', response.url.split('/')[2])
        scrape_item.add_value('data_download_location', s3_key)
        return scrape_item.load_item()
