# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scraper_utilities import formatItemtodb
from scraper_utilities import db_connect
from scraper_utilities import get_logger
import psycopg2
import datetime

logger = get_logger(__name__)


class MedmemeWebscraperPipeline(object):
    def process_item(self, item, spider):
        return item


class PostgresqlPipeline(object):
    """Pipeline that writes scrapy items to postgresql"""
    conn = db_connect()
    cur = conn.cursor()

    def process_item(self, item, spider):
        item_tostring = str(item)
        try:
            self.cur.execute("select seedurl_id from seed_urls where domain_name='%s';" % item['domain_name'][0])
            seedurl_id = self.cur.fetchall()
            for id in seedurl_id:
                self.cur.execute(
                    "INSERT into spider_trace(seedurl_id, spider_name) values('%s','%s')" % (id[0], item['spider'][0]))
            self.cur.execute("UPDATE url_attributes set processmeta=%s where url='%s'" % (formatItemtodb(item_tostring), item['url'][0]))
        except psycopg2.Error as e:
            logger.info(e)

    def open_spider(self, spider):
        spider.started_on = datetime.datetime.now()
        try:
            self.cur.execute("UPDATE spider_trace set spider_opened='%s'" % spider.started_on.isoformat())
        except psycopg2.Error as e:
            logger.info(e)

    def close_spider(self, spider):
        closed_on = datetime.datetime.now()
        work_time = datetime.datetime.now() - spider.started_on
        try:
            self.cur.execute("UPDATE spider_trace set spider_closed='%s'" % (closed_on.isoformat()))
            self.cur.execute("UPDATE spider_trace set total_runtime='%s'" % work_time)
        except psycopg2.Error as e:
            logger.info(e)
        finally:
            self.conn.commit()
            self.cur.close()
            self.conn.close()