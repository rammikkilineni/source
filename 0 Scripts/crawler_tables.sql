-- CREATE TABLES
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'seed_urls'))
BEGIN
	CREATE TABLE dbo.seed_urls(
		[seedurl_id]	UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
		[domain_name]	VARCHAR(200) NOT NULL,
		[url]			VARCHAR(500) NOT NULL,
		[active]		BIT NOT NULL DEFAULT 1,
		[status]		VARCHAR(5) NOT NULL DEFAULT 'NR',
		[dynamic]		BIT NOT NULL DEFAULT 0,
		[company_name]	VARCHAR(500),
		[robots_code]	INT,
		[filter]		VARCHAR(100),
		[manual]		BIT,
		[createdby]		VARCHAR(100),
		[created]		DATETIME,
		[updatedby]		VARCHAR(100),
		[updated]		DATETIME
	 CONSTRAINT [PK_seed_urls_seedurl_id] PRIMARY KEY CLUSTERED
	 (
		[seedurl_id]
	 )
	) ON [PRIMARY]
END
GO

IF (NOT EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.TABLES
                 WHERE TABLE_SCHEMA = 'dbo'
                 AND  TABLE_NAME = 'url_attributes'))
BEGIN
	CREATE TABLE dbo.url_attributes(
		[urlattributes_id]	UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
		[seedurl_id]		UNIQUEIDENTIFIER NOT NULL,
		[download_location]	VARCHAR(500),
		[processmeta]		VARCHAR(500),
		[url]				VARCHAR(500),
		[http_status]		VARCHAR(10),
		[filename]			UNIQUEIDENTIFIER,
		[rundate]			DATETIME NOT NULL,
		[content_length]	INT,
		[last_modified]		VARCHAR(100),
		[createdby]			VARCHAR(100),
		[created]			DATETIME,
		[updatedby]			VARCHAR(100),
		[updated]			DATETIME
	 CONSTRAINT [PK_url_attributes_seedurl_id] PRIMARY KEY CLUSTERED
	 (
		[urlattributes_id]
	 )
	) ON [PRIMARY]
END
GO

IF (NOT EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.TABLES
                 WHERE TABLE_SCHEMA = 'dbo'
                 AND  TABLE_NAME = 'spider_trace'))
BEGIN
	CREATE TABLE dbo.spider_trace(
		[spidertrace_id]	UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
	    [spider_name]		VARCHAR(100),
		[server_name]		VARCHAR(100),
		[spider_opened]		VARCHAR(50),
		[spider_closed]		VARCHAR(50),
		[total_runtime]		VARCHAR(50)
	 CONSTRAINT [PK_spider_trace_seedurl_id] PRIMARY KEY CLUSTERED
	 (
		[spidertrace_id]
	 )
	) ON [PRIMARY]
END
GO