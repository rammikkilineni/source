"""
Objective: Create a tabular output of metadata, given each file/document in the deltacon folder in S3.

Stepwise,

1. Get list of drugbank names into a list from the drugbank folder on S3. Using a Pydrill query.
2. Import each json file/folder from deltacon folder of S3, irrespective of the datasource.
3. Convert complete Json content into a string. (Its currently a dictionary)
4. Parse the string and output the drugnames present/matching in it.
5. Convert the result into a ready made Json in the appropriate format.
6. Export the Json into an S3 folder with the right date and structure.
7. Update the table using deltacon folders and new data.

"""

##Imports and Initializations
import os
from boto3.session import Session
import errno
import re
import json
from pydrill.client import PyDrill
import csv

drugbank_drugs = []
drugbank_ids = []
matches = []
drugs = []

## 1. Get the list of drugbank names and corresponding id's
drill = PyDrill(host='192.168.1.11', port=8047)
# if not drill.is_active():
#     raise ImproperlyConfigured("Please run Drill first")

query_result = drill.query(''' select drugbank_id ,name  from dfs.maprdb.ind   ''', 10000)
for result in query_result:
    for i in result:
        if i == "drugbank_id":
            drugbank_drugs.append(result['name'])
            drugbank_ids.append(result['drugbank_id'])

with open("C:\\Users\\Abhinay Reddy\\Downloads\\drugbank_output_abhi.csv", "r") as f:
    reader = csv.reader(f)
for row in reader:
    for drug in row:
        drugs.append(drug.lower())

## 2. Import each Deltacon folder, along with sourcename

DOWNLOAD_LOCATION_PATH = os.path.expanduser("~") + "/Data/"
if not os.path.exists(DOWNLOAD_LOCATION_PATH):
    print("Making download directory")
os.mkdir(DOWNLOAD_LOCATION_PATH)


def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session


def get_s3_client(new=False):
    session = get_aws_session(new)

    return session.client('s3')


def assert_dir_exists(path):
    """
    Checks if directory tree in path exists. If not it created them.
    :param path: the path to check if it exists
    """
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


bucket = 'nexscope-safety'
path = 'give the S3 directory location'


def download_dir(client, bucket, path, target):
    """
    Downloads recursively the given S3 path to the target directory.
    :param client: S3 client to use.
    :param bucket: the name of the bucket to download from
    :param path: The S3 directory to download.
    :param target: the local directory to download the files to.
    """

    # Handle missing / at end of prefix
    if not path.endswith('/'):
        path += '/'

    paginator = client.get_paginator('list_objects_v2')
    for result in paginator.paginate(Bucket=bucket, Prefix=path):
        print(result)
        # Download each file individually
        for key in result['Contents']:
            # Calculate relative path
            rel_path = key['Key'][len(path):]
            # Skip paths ending in /
            if not key['Key'].endswith('/'):
                local_file_path = os.path.join(target, rel_path)
                # Make sure directories exist
                local_file_dir = os.path.dirname(local_file_path)
                assert_dir_exists(local_file_dir)
                client.download_file(bucket, key['Key'], local_file_path)


client = get_s3_client()

download_dir(client, bucket, path, DOWNLOAD_LOCATION_PATH)


## 3. Convert complete Json file into a string

def json_parse_to_string(file_name):
                raw_data = json.load(open(file_name), encoding="utf8")
                raw_data_string = ', '.join("{!s}={!r}".format(k,v) for (k,v) in raw_data.items())
                clean_string =  re.sub('\W+'," ", raw_data_string )
                return clean_string


#fname = "C:\\Users\\Abhinay Reddy\\Desktop\\sample.json"
fname = "C:\\Users\\Abhinay Reddy\\Desktop\\pubmed_article_29638293.json"
sample = json_parse_to_string(fname)




with open("C:\\Users\\Abhinay Reddy\\Downloads\\drugbank_output_abhi.csv", "r") as f:
    reader = csv.reader(f)
    for row in reader:
        for drug in row:
            drugs.append(drug.lower())

print(drugs)

def db_parser(parsed_content):
    parsed_content = parsed_content.replace(",", "")
    parsed_content = parsed_content.replace(":", "")
    parsed_content = parsed_content.replace(".", "")
    word_list = parsed_content.lower().split()
    matches = [x for x in drugs if x.lower() in word_list]
    return matches

print(db_parser(sample))
print(drugs.index('potassium'))

drugs[0:6]

## 4. Parse the string and output the names present in it

def db_parser_str(parsed_content):
    word_list = parsed_content.lower().split()
    matches = [x for x in drugs if x.lower() in word_list]
    return matches

print(db_parser_str(sample))


## 5. Convert the result into a ready made Json in the appropriate format

#Matches is now a list. We need to convert it into a json file along with other metadata

# a). Get the filename
fname = "C:\\Users\\Abhinay Reddy\\Desktop\\pubmed_article_29638293.json"
file_name = fname.split("\\")[-1].split(".")[0]
print(file_name)

# b). Get the source
all_sources = ["pubmed", "cochrane", "who", "dailymed", "ema", "mhra", "nct"]
for source in all_sources:
    if source in file_name:
        abstract_source = source

print(abstract_source)

# c). Get the druglist and ids associated
drug_list = db_parser(sample)
print("drug list is:{}".format(drug_list))
for drug in drug_list:
    drugbank_ids.append(drugs[drugs.index(drug) - 1])
print(drugbank_ids)

# d). Let us convert this into a Json, by first getting an appropriate dictionary

dict_keys = ['filename','source','drugnames','drugbank_ids']
print(dict_keys)
dict_values = [file_name, abstract_source, drug_list, drugbank_ids]
print(dict_values)

abstract_dictionary = dict(zip(dict_keys, dict_values))
print(abstract_dictionary)

import json
with open('result.json', 'w') as fp:
    json.dump(abstract_dictionary, fp)
#We have to rename the 'result.json' to something more organized

## 6. Export the Json into S3 in the current folder, with the right data and structure

#This is currently uploading from one of Abhinay's local directories to 'data/2018/06/07/drugbank_parse_uploads/' in the nexscope-safety bucket
import os
from boto3.session import Session


def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session


def get_s3_client(new=False):
    session = get_aws_session(new)

    return session.client('s3')


# get an access token, local (from) directory, and S3 (to) directory
# from the command-line
local_directory = "C:\\Users\\Abhinay Reddy\\PycharmProjects\\Production_Pieces\\db_parse_results"
bucket = 'nexscope-safety'
destination = 'data/2018/06/07/drugbank_parse_uploads/'

client = get_s3_client()

# enumerate local files recursively
for root, dirs, files in os.walk(local_directory):

    for filename in files:

        # construct the full local path
        local_path = os.path.join(root, filename)

        # construct the full Dropbox path
        relative_path = os.path.relpath(local_path, local_directory)
        s3_path = os.path.join(destination, relative_path)

        # relative_path = os.path.relpath(os.path.join(root, filename))

        print('Searching "%s" in "%s"' % (s3_path, bucket))
        try:
            client.head_object(Bucket=bucket, Key=s3_path)
            print("Path found on S3! Skipping %s..." % s3_path)

            # try:
            # client.delete_object(Bucket=bucket, Key=s3_path)
            # except:
            # print "Unable to delete %s..." % s3_path
        except:
            print("Uploading %s..." % s3_path)
            client.upload_file(local_path, bucket, s3_path)


######### 7. Deltacon updating of the folders in S3

#This has to be looked upon later, I have to discuss the logic with someone.