from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from pprint import pprint
import time
import json
import os
from boto3.session import Session
import errno

options = webdriver.ChromeOptions()

MeSH_terms = []
data = {}
actual_terms = []

bucket = 'nexscope-safety'
path = 'pubmed_sindhu/'


def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session

def get_s3_client(new=False):
    session = get_aws_session(new)
    return session.client('s3')

def assert_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def download_dir(client, bucket, path, target):
    if not path.endswith('/'):
        path += '/'
    paginator = client.get_paginator('list_objects_v2')
    for result in paginator.paginate(Bucket=bucket, Prefix=path):
        print(result)
        # Download each file individually
        for key in result['Contents']:
            # Calculate relative path
            rel_path = key['Key'][len(path):]
            # Skip paths ending in /
            if not key['Key'].endswith('/'):
                local_file_path = os.path.join(target, rel_path)
                # Make sure directories exist
                local_file_dir = os.path.dirname(local_file_path)
                assert_dir_exists(local_file_dir)
                client.download_file(bucket, key['Key'], local_file_path)

def call_website(DOWNLOAD_LOCATION_PATH):

    for filename in os.listdir(DOWNLOAD_LOCATION_PATH):
        if filename.endswith(".json"):
            with open(DOWNLOAD_LOCATION_PATH + "%s" % filename) as json_data:
                data = json.load(json_data)
                input_string = data["article_title"]
                for abs in data["abstract_text"]:
                    input_string = input_string + abs['text']
                if 'pubmed' in filename:
                    source_name = "Pubmed"
                elif 'CT' in filename:
                    source_name = "Cochrane"
                elif 'who' in filename:
                    source_name = "WHO"

                driver = webdriver.Chrome(chrome_options=options)
                driver.get('https://meshb.nlm.nih.gov/MeSHonDemand')

                text_area = driver.find_element_by_id('MODEntry')
                text_area.send_keys(input_string)
                driver.find_element_by_id('runMOD').click()

                time.sleep(60)

                for elem in driver.find_elements_by_xpath('.//span[@class = "highlightedModTerm modHighlight"]'):
                    element_to_hover_over = elem
                    hover = ActionChains(driver).move_to_element(element_to_hover_over)
                    hover.perform()
                    for term in driver.find_elements_by_xpath('.//a[@class = "highlightedModTerm hoveredModTerm ng-binding ng-scope redClass"]'):
                        actual_terms.append(term.text)
                        for term in actual_terms:
                            if term not in MeSH_terms:
                                MeSH_terms.append(term)

            current_directory = os.getcwd()
            directory = os.path.join(current_directory, r'JSON_OUTPUTS/')
            if not os.path.exists(directory):
                os.mkdir(directory)
            file = os.path.join(directory, "result_MeSH.json")

            with open(file, 'w') as outfile:
                json.dump({"Filename": filename,
                           "Source Name": source_name,
                           "MeSH Terms": MeSH_terms
                     }, outfile, indent=4)

def write_to_s3():

    current_directory = os.getcwd()
    local_directory = os.path.join(current_directory, r'JSON_OUTPUTS/')

    bucket = 'nexscope-safety'
    destination = 'JSON_OUTPUTS_1/'

    client = get_s3_client()

    for root, dirs, files in os.walk(local_directory):
        for filename in files:
            local_path = os.path.join(root, filename)
            relative_path = os.path.relpath(local_path, local_directory)
            s3_path = os.path.join(destination, relative_path)
            print('Searching "%s" in "%s"' % (s3_path, bucket))
        try:
            client.head_object(Bucket=bucket, Key=s3_path)
            print("Path found on S3! Skipping %s..."% s3_path)
        except:
            print("Uploading %s..." % s3_path)
            client.upload_file(local_path, bucket, s3_path)

def get_download_location():

    current_directory = os.getcwd()
    # DOWNLOAD_LOCATION_PATH = os.path.expanduser("~") + "/Data/"
    DOWNLOAD_LOCATION_PATH = os.path.join(current_directory, r'Data/')
    if not os.path.exists(DOWNLOAD_LOCATION_PATH):
        print("Making download directory")
        os.mkdir(DOWNLOAD_LOCATION_PATH)

    return DOWNLOAD_LOCATION_PATH
def main():

    DOWNLOAD_LOCATION_PATH = get_download_location()
    client = get_s3_client()
    download_dir(client, bucket, path, DOWNLOAD_LOCATION_PATH)
    call_website(DOWNLOAD_LOCATION_PATH)

    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
    if not os.path.exists(directory):
        os.mkdir(directory)
    file = os.path.join(directory, "MESH_OUTPUT.txt")
    f1 = open(file, "w")

    for t in MeSH_terms:
        f1.write(t)
        f1.write("\n")

