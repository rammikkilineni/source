import MeSH
import MEDDRA
import MeSH_MEDD_Map
import MEDDRA_UNION

MeSH.main()
MeSH.write_to_s3()
MEDDRA.main()
MEDDRA.write_to_s3()
MeSH_MEDD_Map.main()
MEDDRA_UNION.main()
