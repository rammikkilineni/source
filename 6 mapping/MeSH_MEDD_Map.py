import urllib.response
import urllib.request
import urllib.parse
import json
import re
import os
import sys

REST_URL = "http://data.bioontology.org"
API_KEY = "6e00a808-51d9-4381-a593-787674286a98"

mesh_link_list = []
classes_terms = []
mesh_terms_final = []
mesh_links_final = []
my_dict = {}

def get_json(url):
    opener = urllib.request.build_opener()
    opener.addheaders = [('Authorization', 'apikey token=' + API_KEY)]
    url1 = url + "?include=all"
    return json.loads(opener.open(url1).read())

def print_mappings():
    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
    file = os.path.join(directory, "MeSH_MEDDRA_OUTPUT.txt")
    f1 = open(file, "w")
    for link in mesh_link_list:
        classes = get_json(REST_URL + '/search?q=' + urllib.parse.quote(link) + "&longest_only=false&exclude_numbers=false&whole_word_only=true&exclude_synonyms=false?include=all")
        for result in classes["collection"]:
            search_data = result["links"]["mappings"]
            if "MESH" in search_data:
                    mappings = get_json(search_data)
                    for result in mappings:
                        for res1 in result["classes"]:
                            if "MEDDRA" in res1["links"]["ontology"]:
                                res2 = get_json(res1["links"]["self"])
                                my_list = []
                                f1.write("Term : " + res2["prefLabel"])
                                f1.write("\n")
                                my_dict[res2["prefLabel"]] = []
                                f1.write("Term Notation : " + res2["notation"])
                                f1.write("\n")
                                my_list.append({"Term_Notation": res2["notation"]})
                                for res1, val in res2["properties"].items():
                                    if "classified_as" in res1:
                                        for val1 in val:
                                            val2 = val1.rsplit('/', 1)[-1]
                                            res3 = res2["links"]["self"]
                                            res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                                            res5 = res3.replace(res4[-1], val2)
                                            res6 = get_json(res5)
                                            notation = res6["notation"]
                                            f1.write("Classified as : " + res6["prefLabel"])
                                            f1.write("\n")
                                            my_list.append({"Classified_as": res6["prefLabel"]})
                                            f1.write("Classified as Notation : " + notation)
                                            f1.write("\n")
                                            my_list.append({"Classified_as_Notation": notation})
                                            for res7, val in res6["properties"].items():
                                                if "classifies" in res7:
                                                    for val1 in val:
                                                        val2 = val1.rsplit('/', 1)[-1]
                                                        res7 = res2["links"]["self"]
                                                        res8 = re.split('[a-f]+', res7, flags=re.IGNORECASE)
                                                        res9 = res7.replace(res8[-1], val2)
                                                        res10 = get_json(res9)
                                                        notation10 = res10["notation"]
                                                        f1.write("Classifies : " + res10["prefLabel"])
                                                        f1.write("\n")
                                                        my_list.append({"Classifies": res10["prefLabel"]})
                                                        f1.write("Classifies Notation : " + notation10)
                                                        f1.write("\n")
                                                        my_list.append({"Classifies_Notation": notation10})
                                    elif "classifies" in res1:
                                        for val1 in val:
                                            val2 = val1.rsplit('/', 1)[-1]
                                            res3 = res2["links"]["self"]
                                            res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                                            res5 = res3.replace(res4[-1], val2)
                                            res6 = get_json(res5)
                                            notation = res6["notation"]
                                            f1.write("Classifies : " + res6["prefLabel"])
                                            f1.write("\n")
                                            my_list.append({"Classifies": res6["prefLabel"]})
                                            f1.write("Classifies Notation : " + notation)
                                            f1.write("\n")
                                            my_list.append({"Classifies_Notation": notation})

                                my_dict[res2["prefLabel"]] = my_list

            elif "MESH" not in search_data:
                continue

        return(my_dict)

def main():

    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
    file = os.path.join(directory, "MESH_OUTPUT.txt")
    f = open(file, "r")
    mesh_terms = f.readlines()

    for term in mesh_terms:
        term2 = term.replace('\n', '')
        mesh_link_list.append(term2)

        print_mappings()



