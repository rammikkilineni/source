import MEDDRA
import MeSH_MEDD_Map
import json
import os
from boto3.session import Session

def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session

def get_s3_client(new=False):
    session = get_aws_session(new)
    return session.client('s3')

def write_to_s3():
    current_directory = os.getcwd()
    local_directory = os.path.join(current_directory, r'JSON_OUTPUTS/')

    bucket = 'nexscope-safety'
    destination = 'JSON_OUTPUTS_1/'
    client = get_s3_client()
    for root, dirs, files in os.walk(local_directory):
        for filename in files:
            local_path = os.path.join(root, filename)
            relative_path = os.path.relpath(local_path, local_directory)
            s3_path = os.path.join(destination, relative_path)
            print('Searching "%s" in "%s"' % (s3_path, bucket))
            try:
                client.head_object(Bucket=bucket, Key=s3_path)
                print("Path found on S3! Skipping %s..."% s3_path)
            except:
                print("Uploading %s..." % s3_path)
                client.upload_file(local_path, bucket, s3_path)

def main():

    MEDDRA.main()
    meddra_terms = MEDDRA.print_properties()
    MeSH_MEDD_Map.main()
    meddra_map_terms = MeSH_MEDD_Map.print_mappings()
    union_dict = meddra_map_terms

    call_filename = MEDDRA.call_input()
    filename = call_filename["filename"]
    source_name = call_filename["source_name"]

    # if meddra_map_terms is not None:
    for term, val in meddra_terms.items():
        if term not in meddra_map_terms:
            union_dict[term] = val
    # else:
    #     union_dict = meddra_terms

    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
    file = os.path.join(directory, "Unique_MEDDRA_Terms.txt")
    f1 = open(file, "w")

    for key, val in union_dict.items():
        f1.write("Term : " + key)
        f1.write("\n")
        for i in val:
            for k, v in i.items():
                if k == "Term_Notation":
                    f1.write("Term_Notation : " + v)
                    f1.write("\n")
                if k == "Classified_as":
                    f1.write("Classified_as : " + v)
                    f1.write("\n")
                if k == "Classified_as_Notation":
                    f1.write("Classified_as_Notation : " + v)
                    f1.write("\n")
                if k == 'Classifies':
                    f1.write("Classifies : " + v)
                    f1.write("\n")
                if k == "Classifies_Notation":
                    f1.write("Classifies_Notation : " + v)
                    f1.write("\n")

    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'JSON_OUTPUTS/')

    file = os.path.join(directory, "result_Unique_Meddra.json")

    with open(file, 'w') as outfile:
        json.dump({"Filename": filename,
                   "Source Name": source_name,
                   "Final MEDDRA Terms": union_dict
                }, outfile, indent=4)

        write_to_s3()

