import urllib.response
import urllib.request
import urllib.parse
import json
import re
import unicodedata
import os
from boto3.session import Session

REST_URL = "http://data.bioontology.org"
API_KEY = "6e00a808-51d9-4381-a593-787674286a98"

result_url = []
classes_terms = []
search_results = []
MEDDRA_terms = []
classes_dict = dict()
classified_as_dict = dict()
classifies_dict = dict()
result_dict = dict()

my_dict = {}

def print_annotations(annotations, get_class = True):
    for result in annotations:
        class_details = get_json(result["annotatedClass"]["links"]["self"]) if get_class else result["annotatedClass"]
        classes_terms.append(class_details)
        classes_notation = class_details["notation"]
        classes_dict[class_details["prefLabel"]] = classes_notation

def print_properties():
    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
    file = os.path.join(directory, "MEDDRA_OUTPUT.txt")
    f1 = open(file, "w")
    for result in classes_terms:
        my_list = []
        f1.write("Term : " + result["prefLabel"])
        my_dict[result["prefLabel"]] = []
        f1.write("\n")
        f1.write("Term Notation : " + result["notation"])
        my_list.append({"Term_Notation": result["notation"]})
        f1.write("\n")
        for res1, val in result["properties"].items():
            if "classified_as" in res1:
                for val1 in val:
                    val2 = val1.rsplit('/', 1)[-1]
                    res3 = result["links"]["self"]
                    res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                    res5 = res3.replace(res4[-1], val2)
                    res6 = get_json(res5)
                    notation = res6["notation"]
                    f1.write("Classified as : " + res6["prefLabel"])
                    my_list.append({"Classified_as": res6["prefLabel"]})
                    f1.write("\n")
                    f1.write("Classified as Notation : " + notation)
                    my_list.append({"Classified_as_Notation": notation})
                    f1.write("\n")
                    for res7, val in res6["properties"].items():
                        if "classifies" in res7:
                            for val1 in val:
                                val2 = val1.rsplit('/', 1)[-1]
                                res7 = result["links"]["self"]
                                res8 = re.split('[a-f]+', res7, flags=re.IGNORECASE)
                                res9 = res7.replace(res8[-1], val2)
                                res10 = get_json(res9)
                                notation10 = res10["notation"]
                                f1.write("Classifies : " + res10["prefLabel"])
                                my_list.append({"Classifies": res10["prefLabel"]})
                                f1.write("\n")
                                f1.write("Classifies Notation : " + notation10)
                                my_list.append({"Classifies_Notation": notation10})
                                f1.write("\n")
            elif "classifies" in res1:
                for val1 in val:
                    val2 = val1.rsplit('/', 1)[-1]
                    res3 = result["links"]["self"]
                    res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                    res5 = res3.replace(res4[-1], val2)
                    res6 = get_json(res5)
                    notation = res6["notation"]
                    f1.write("Classifies : " + res6["prefLabel"])
                    f1.write("\n")
                    my_list.append({"Classifies": res6["prefLabel"]})
                    f1.write("Classifies Notation : " + notation)
                    my_list.append({"Classifies_Notation": notation})
                    f1.write("\n")

        my_dict[result["prefLabel"]] = my_list

    return (my_dict)

def getontologyforfilter():
    return "MEDDRA"

def call_input():

    call_input_res = {}
    for filename in os.listdir("C:\\Users\\Sindhu Boppudi\\Data"):
        if filename.endswith(".json"):
            with open("C:\\Users\\Sindhu Boppudi\\Data\\%s" % filename) as json_data:
                data = json.load(json_data)
                input_string = data["article_title"]
                for abs in data["abstract_text"]:
                    input_string = input_string + abs['text']
                if 'pubmed' in filename:
                    source_name = "Pubmed"
                elif 'CT' in filename:
                    source_name = "Cochrane"
                elif 'who' in filename:
                    source_name = "WHO"

        call_input_res["input_string"] = input_string
        call_input_res["filename"] = filename
        call_input_res["source_name"] = source_name

    return call_input_res

def get_json(url):
    opener = urllib.request.build_opener()
    opener.addheaders = [('Authorization', 'apikey token=' + API_KEY)]
    url1 = url + "?include=all"
    return json.loads(opener.open(url1).read())

def main():

    call_input_result = call_input()
    raw_text = call_input_result["input_string"]
    text_to_annotate = unicodedata.normalize('NFKD', raw_text).encode('ascii', 'ignore')
    annotations = get_json(REST_URL + "/annotator?text=" + urllib.parse.quote(text_to_annotate) + "&ontologies=" + getontologyforfilter() + "&longest_only=false&exclude_numbers=false&whole_word_only=true&exclude_synonyms=false?include=all")
    print_annotations(annotations)
    print_properties()

    current_directory = os.getcwd()
    directory = os.path.join(current_directory, r'JSON_OUTPUTS/')

    file = os.path.join(directory, "result_MEDDRA.json")

    with open(file, 'w') as outfile:
        json.dump({"Filename": call_input_result["filename"],
                   "Source Name": call_input_result["source_name"],
                   "MEDDRA Terms": my_dict
               }, outfile, indent=4)

def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'
    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )
    return session


def get_s3_client(new=False):
    session = get_aws_session(new)
    return session.client('s3')

def write_to_s3():

    current_directory = os.getcwd()
    local_directory = os.path.join(current_directory, r'JSON_OUTPUTS/')
    bucket = 'nexscope-safety'
    destination = 'JSON_OUTPUTS_1/'
    client = get_s3_client()
    for root, dirs, files in os.walk(local_directory):
        for filename in files:
            local_path = os.path.join(root, filename)
            relative_path = os.path.relpath(local_path, local_directory)
            s3_path = os.path.join(destination, relative_path)
            print('Searching "%s" in "%s"' % (s3_path, bucket))
            try:
                client.head_object(Bucket=bucket, Key=s3_path)
                print("Path found on S3! Skipping %s..."% s3_path)
            except:
                print("Uploading %s..." % s3_path)
                client.upload_file(local_path, bucket, s3_path)
