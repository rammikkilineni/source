﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CrawlerService.Models
{
    public class MedContext : DbContext
    {
        public MedContext(DbContextOptions<MedContext> options)
            : base(options)
        {

        }

        public DbSet<seed_urls> URLs { get; set; }
    }
}
