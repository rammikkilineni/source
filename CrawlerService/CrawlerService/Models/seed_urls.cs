﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrawlerService.Models
{
    public class seed_urls
    {
        public Guid seedurl_id { get; set; }

        public string domain_name { get; set; }

        public string url { get; set; }

        public int active { get; set; }

        public string status { get; set; }

        public int dynamic { get; set; }

        public string company_name { get; set; }

        public int robots_code { get; set; }

        public string filter { get; set; }
    }
}